package Estructura;

/**
 * @author Juan Diaz - FuriosoJack <http://blog.furiosojack.com/>
 *
 * 
 */
public interface Resultado {

	/**
	 * Retorna el mensaje del resultado final
	 * @return String resultado
	 */
	public String getResultado();
	
	public void run();
}
