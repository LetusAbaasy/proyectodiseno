package ArbolBinario;

public interface UnNodo {
	public UnNodo getHijoIzq();

	public void setHijoIzq(UnNodo hijoIzq);

	public UnNodo getHijoDer();

	public void setHijoDer(UnNodo hijoDer);

	public UnNodo getPadre();

	public void setPadre(UnNodo padre);

	public int getValor();

	public int getKey();

	public void setKey(int key);

	public void setValor(int valor);
}
