package ArbolBinario;

/**
 * @author Juan Diaz - FuriosoJack <http://blog.furiosojack.com/>
 *
 * 
 */
public class Nodo {

	private Nodo hijoIzq;
	private Nodo hijoDer;
	private Nodo padre;
	private int valor;
	private int key;

	public Nodo(int key) {
		this.key = key;
	}

	public Nodo getHijoIzq() {
		return hijoIzq;
	}

	public void setHijoIzq(Nodo hijoIzq) {
		this.hijoIzq = hijoIzq;
	}

	public Nodo getHijoDer() {
		return hijoDer;
	}

	public void setHijoDer(Nodo hijoDer) {
		this.hijoDer = hijoDer;
	}

	public Nodo getPadre() {
		return padre;
	}

	public void setPadre(Nodo padre) {
		this.padre = padre;
	}

	public int getValor() {
		return valor;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
	
	

}
