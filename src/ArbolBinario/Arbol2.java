package ArbolBinario;

import java.util.ArrayList;
import java.util.List;

public class Arbol2 {

	private Nodo2 raiz;

	private int[] lista;
	private List<Nodo2> ordenada;
	
	public Arbol2() {
		this.ordenada = new ArrayList<Nodo2>();
	}

	public Nodo2 getRaiz() {
		return raiz;
	}

	public void setRaiz(Nodo2 raiz) {
		this.raiz = raiz;
	}

	public void addNodo2(int valor, int persona, int trabajo) {
		Nodo2 Nodo2tmp = new Nodo2(valor,persona,trabajo);

		if (this.raiz != null) {
			Nodo2 elNodo2 = this.raiz;
			while (elNodo2 != null) {
				// Reviso si va para la izquierda o derecha
				if (elNodo2.getKey() > valor) {
					// Se revisa el siguiente Nodo2
					if (elNodo2.getHijoIzq() != null) {
						// Se reempleza el por Nodo2 hijo
						elNodo2 = elNodo2.getHijoIzq();
					} else {
						// Como no hay nada se pone el nuevo Nodo2
						// Se le agrega el Nodo2 padre al Nodo2
						Nodo2tmp.setPadre(elNodo2);
						elNodo2.setHijoIzq(Nodo2tmp);

						break;
					}
				} else {
					if (elNodo2.getHijoDer() != null) {
						elNodo2 = elNodo2.getHijoDer();
					} else {
						Nodo2tmp.setPadre(elNodo2);
						elNodo2.setHijoDer(Nodo2tmp);
						break;
					}
				}
			}

		} else {
			this.raiz = Nodo2tmp;
		}

	}


	public void inOrden(Nodo2 nodo) {
		if (nodo != null) {
			inOrden(nodo.getHijoIzq());
			this.ordenada.add(nodo);
			//System.out.println(nodo.getValor());
			inOrden(nodo.getHijoDer());
		}
	}
	
	public List<Nodo2> getOrdenada()
	{
		return this.ordenada;
	}
}
