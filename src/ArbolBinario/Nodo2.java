package ArbolBinario;

public class Nodo2 {

	private int persona;
	private int trabajo;
	private Nodo2 hijoIzq;
	private Nodo2 hijoDer;
	private Nodo2 padre;
	private int valor;
	private int key;
	public Nodo2(int key, int persona, int trabajo) {
		this.key = key;
		this.persona = persona;
		this.trabajo = trabajo;
		// TODO Auto-generated constructor stub
	}
	public int getPersona() {
		return persona;
	}
	public void setPersona(int persona) {
		this.persona = persona;
	}
	public int getTrabajo() {
		return trabajo;
	}
	public void setTrabajo(int trabajo) {
		this.trabajo = trabajo;
	}

	public Nodo2(int key) {
		this.key = key;
	}

	public Nodo2 getHijoIzq() {
		return hijoIzq;
	}

	public void setHijoIzq(Nodo2 hijoIzq) {
		this.hijoIzq = hijoIzq;
	}

	public Nodo2 getHijoDer() {
		return hijoDer;
	}

	public void setHijoDer(Nodo2 hijoDer) {
		this.hijoDer = hijoDer;
	}

	public Nodo2 getPadre() {
		return padre;
	}

	public void setPadre(Nodo2 padre) {
		this.padre = padre;
	}

	public int getValor() {
		return valor;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}
