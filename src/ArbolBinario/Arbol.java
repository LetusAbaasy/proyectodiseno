package ArbolBinario;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Juan Diaz - FuriosoJack <http://blog.furiosojack.com/>
 *
 * 
 */
public class Arbol {

	private Nodo raiz;

	private int[] lista;
	private List<Integer> ordenada;
	
	public Arbol() {
		this.ordenada = new ArrayList<Integer>();
	}

	public Nodo getRaiz() {
		return raiz;
	}

	public void setRaiz(Nodo raiz) {
		this.raiz = raiz;
	}

	public void addNodo(int valor) {
		Nodo nodotmp = new Nodo(valor);

		if (this.raiz != null) {
			Nodo elnodo = this.raiz;
			while (elnodo != null) {
				// Reviso si va para la izquierda o derecha
				if (elnodo.getKey() > valor) {
					// Se revisa el siguiente nodo
					if (elnodo.getHijoIzq() != null) {
						// Se reempleza el por nodo hijo
						elnodo = elnodo.getHijoIzq();
					} else {
						// Como no hay nada se pone el nuevo nodo
						// Se le agrega el nodo padre al nodo
						nodotmp.setPadre(elnodo);
						elnodo.setHijoIzq(nodotmp);

						break;
					}
				} else {
					if (elnodo.getHijoDer() != null) {
						elnodo = elnodo.getHijoDer();
					} else {
						nodotmp.setPadre(elnodo);
						elnodo.setHijoDer(nodotmp);
						break;
					}
				}
			}

		} else {
			this.raiz = nodotmp;
		}

	}

	public void setLista(int[] lista) {
		/// agrega la lista al arbol
		this.lista = new int[lista.length];
		
		for (int i = 0; i < lista.length; i++) {
			this.addNodo(lista[i]);			
		}

	}

	public void inOrden(Nodo nodo) {
		if (nodo != null) {
			inOrden(nodo.getHijoIzq());
			this.ordenada.add(nodo.getKey());
			//System.out.println(nodo.getValor());
			inOrden(nodo.getHijoDer());
		}
	}
	
	public List<Integer> getOrdenada()
	{
		return this.ordenada;
	}
}
