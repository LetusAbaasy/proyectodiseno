package Main;

import Downloader.FileDownloaderNEW;
import Programas.BacktraingConArbol;
import Programas.Backtraking;
import Programas.DivideYVenceras;
import Programas.ProgramacionDinamica;

import javax.swing.*;

import Ayuda.Ayuda;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	private JFrame frame;
	private JToolBar toolBar;
	private JPopupMenu popup;
	private JPopupMenu popupAyuda;
	private String iconPath;

	/**
	 * Esto es el metodo main donde se cargara el men�.
	 * 
	 * @throws Exception
	 */
	public Main() throws Exception {

		this.cargarIcono();

		// Creamos el Frame
		this.frame = new JFrame("Proyecto Final");
		this.frame.setPreferredSize(new Dimension(270, 80));

		this.toolBar = new JToolBar();
		// Create the popup menu.
		this.popup = new JPopupMenu();
		// Create the popup menu.
		this.popupAyuda = new JPopupMenu();

		this.cargarToolBar();
		this.cargarPopoup();
		this.cargarFrame();

	}

	private void cargarIcono() {
		String appdata = System.getenv("APPDATA");
		this.iconPath = appdata + "\\JAPP_icon.png";
		File icon = new File(iconPath);
		// Para descargar el icono de la aplicacion
		try {
			while (!icon.exists()) {
				FileDownloaderNEW fd = new FileDownloaderNEW();
				fd.download(
						"http://icons.iconarchive.com/icons/blackvariant/button-ui-requests-15/256/MusicBrainz-icon.png",
						this.iconPath, false, false);
			}
		} catch (Exception e) {
			// System.out.println(e.getMessage());
			// Recursion para no volver a instanciar la clase
			this.cargarIcono();
		}

	}

	private void cargarToolBar() {
		// Boton en toolbar
		final JButton button = new JButton("Programas");
		final JButton ayuda = new JButton("?");

		// Evento Mouse
		button.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});

		ayuda.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				popupAyuda.show(e.getComponent(), e.getX(), e.getY());
			}
		});

		// A�adirmos los botones al toolbar
		toolBar.add(button);
		toolBar.add(ayuda);
	}

	private void cargarPopoup() {
		// Primer Programa
		this.popup.add(new JMenuItem(new AbstractAction("Dividir y Vencer") {
			public void actionPerformed(ActionEvent e) {

				int cantidad = Integer.parseInt(JOptionPane.showInputDialog(frame, "¿Cantidad de numeros?"));
				int[] vector = new int[cantidad];

				for (int i = 0; i < vector.length; i++) {
					int numero = Integer.parseInt(JOptionPane.showInputDialog(frame, "Ingrese el numero " + (i + 1)));
					vector[i] = numero;
				}
				System.out.println(Arrays.toString(vector));
				DivideYVenceras alg = new DivideYVenceras(vector);
				alg.run();
				JOptionPane.showMessageDialog(frame, alg.getResultado());

			}
		}));

		// Segundo Programa
		this.popup.add(new JMenuItem(new AbstractAction("Programacion Dinamica") {
			public void actionPerformed(ActionEvent e) {
				ProgramacionDinamica PD = new ProgramacionDinamica();

				int cantidadEmbarcaderos = Integer.parseInt(JOptionPane.showInputDialog(frame, "�Cantidad de embarcaderos?"));
				int[][] embarcaderos = new int[cantidadEmbarcaderos][cantidadEmbarcaderos];

				for (int i = 0; i < embarcaderos.length; i++) {
					for (int j = 0; j < embarcaderos[i].length; j++) {
						if(i >= j) {
							embarcaderos[i][j] = 0;
						}else {
							int costeEmbarcadero = Integer.parseInt(JOptionPane.showInputDialog(frame,"�Coste del embarcadero desde el #" + (i + 1) + " al #" + (j + 1) + "?"));
							embarcaderos[i][j] = costeEmbarcadero;
						}
					}
				}

				int embarcaderoSalida = Integer.parseInt(JOptionPane.showInputDialog(frame, "�Embarcadero de salida?"));
				/*
				 * try{ while(embarcaderoSalida > embarcaderos.length || embarcaderoSalida <
				 * embarcaderos.length-1){ JOptionPane.showMessageDialog(frame,
				 * "El numero del embarcadero de salida no existe, vuelva a ingresar el embarcadero de salida."
				 * ); } }catch (Exception e1) { System.out.println(e1.getMessage()); }
				 */

				int embarcaderoLlegada = Integer.parseInt(JOptionPane.showInputDialog(frame, "�Embarcadero de llegada?"));
				/*
				 * try{ while(embarcaderoLlegada > embarcaderos.length || embarcaderoLlegada <
				 * embarcaderos.length-1){ JOptionPane.showMessageDialog(frame,
				 * "El numero del embarcadero de salida no existe, vuelva a ingresar el embarcadero de salida."
				 * ); } }catch (Exception e1) { System.out.println(e1.getMessage()); }
				 */
				PD.ProgramacionDinamica(embarcaderos, embarcaderoSalida, embarcaderoLlegada, cantidadEmbarcaderos);
			}
		}));

		// Tercer Programa
		this.popup.add(new JMenuItem(new AbstractAction("Vuelta Atras") {
			public void actionPerformed(ActionEvent e) {
				//VueltaAtras VA = new VueltaAtras();
				
				int cantidadPersonas = Integer.parseInt(JOptionPane.showInputDialog(frame, "¿Cantidad de personas y trabajos?"));
				
				
				int[][] matrizTareas = new int[cantidadPersonas][cantidadPersonas];

				for (int i = 0; i < matrizTareas.length; i++) {
					for (int j = 0; j < matrizTareas[i].length; j++) {
						int costeTarea = Integer.parseInt(JOptionPane.showInputDialog(frame,"¿Coste para la persona #" + (i + 1) + " y persona #" + (j + 1) + "?"));
						matrizTareas[i][j] = costeTarea;
					}
				}
				//System.out.println(Arrays.toString(matrizTareas));
				//Backtraking back = new Backtraking(cantidadPersonas,matrizTareas);
				//back.asignacion(0);
				BacktraingConArbol back = new BacktraingConArbol(matrizTareas);
				back.run();
				
			}
		}));

		this.popupAyuda.add(new JMenuItem(new AbstractAction("Texto de los problemas") {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, Ayuda.TXT_AYUDA);
			}
		}));
	}

	private void cargarFrame() {
		JLabel lblHeading = new JLabel("De clic en un programa.");
		lblHeading.setFont(new Font("Arial", Font.TRUETYPE_FONT, 12));
		// Cargamos todo el frame
		frame.getContentPane().add(lblHeading, BorderLayout.SOUTH);
		frame.getContentPane().add(toolBar, BorderLayout.NORTH);
		// Imagen icon de la aplicacion
		ImageIcon imgicon = new ImageIcon(iconPath);
		frame.setIconImage(imgicon.getImage());
		// Opciones del frame
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}

	// Main aplicaci�n
	public static void main(String[] args) throws Exception {
		Main main = new Main();

	}
}
