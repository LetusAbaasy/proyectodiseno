package Ayuda;

public class Ayuda {
	public static final String TXT_AYUDA = 
				"DIVIDIR Y VENCER \n"
				+ "Sea A[1…n] un vector de enteros. Un elemento x se denomina elemento mayoritario de A si x aparece en el vector más de n/2 veces, es decir, \n"
				+ "Card {i | A[i]=x} > n/2. A través de una estrategia dividir y vencer, implemente un algoritmo capaz de decidir si un vector dado contiene un elemento mayoritario \n"
				+ "(no puede haber más de uno). El tamaño del vector y los valores del mismo serán cargados por el usuario. \n\n"
				+ "PROGRAMACIÓN DINÁMICA \n"
				+ "Sobre un río hay n embarcaderos. En cada uno de ellos se puede alquilar un bote que permite ir a cualquier otro embarcadero río abajo (es imposible ir río arriba). \n"
				+ "Existe una tabla de tarifas que indica el coste del viaje del embarcadero i al j para cualquier embarcadero de partida i y cualquier embarcadero de llegada j más \n"
				+ "abajo en el río (i < j). Puede suceder que un viaje de i a j sea más caro que una sucesión de viajes más cortos, en cuyo caso se tomaría un primer bote hasta un \n"
				+ "embarcadero k y un segundo bote para continuar a partir de k. No hay coste adicional por cambiar de bote. Diseñe un algoritmo basado en una estrategia de programación \n"
				+ "dinámica que determine el coste mínimo para ir de un punto i a un punto j. El usuario podrá cargar el número de embarcaderos y los diferentes costes en una tabla, \n"
				+ "así como también el embarcadero de partida y el de llegada, y el algoritmo dará como respuesta qué embarcaderos incluirá la ruta y cuál es su costo. \n\n"
				+ "VUELTA ATRÁS \n"
				+ "Dadas n personas y n tareas, se desea asignar a cada persona una tarea. El coste de asignar a la persona i la tarea j viene determinado por la posición [i,j] \n"
				+ "de una matriz dada (TARIFAS). Diseñar un algoritmo, basado en una estrategia de vuelta atrás, que asigne una tarea a cada persona minimizando el coste de la asignación. \n "
				+ "Por ejemplo, si consideramos tres personas a, b y c, a los que hay que asignar las tareas 1,2 y 3 con la matriz de costes siguiente: "
				+ "\n 1 2 3 a 4 7 3 b 2 6 1 c 3 9 4 \n\n "
				+ "Se podría asignar la tarea 1 a la persona a, la tarea 2 a la persona b y la tarea 3 a la persona c, entonces nuestro coste total será 4+6+4=14, \n"
				+ "mientras que si asignamos la tarea 3 a la persona a, la tarea 2 a la persona b y la tarea 1 a la persona c, el coste será solamente 3+6+3=12. \n"
				+ "Se puede verificar entonces, que la asignación óptima es a→2, b→3 y c→1, cuyo coste es 7+1+3=11. El usuario podrá cargar la matriz TARIFAS y el programa dará \n"
				+ "como respuesta la asignación de las tareas a cada una de las personas.\n\n"
				+ "Estudiantes:\n\n"
				+ "Juan Díaz - Sergio Martínez";

}
