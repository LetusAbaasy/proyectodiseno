package Programas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import ArbolBinario.Arbol2;
import ArbolBinario.Nodo2;

public class BacktraingConArbol {


	private int[][] matrix;
	private Arbol2 elarboL;
	public BacktraingConArbol(int[][] matrix)
	{
		this.matrix = matrix;
		this.elarboL = new Arbol2();
	}
	
	
	public void run()
	{
		this.armarArbol();
		this.elarboL.inOrden(this.elarboL.getRaiz());
		List<Nodo2> ordenados = this.elarboL.getOrdenada();
		List<Nodo2> mejores = this.mejor(ordenados);
		for (Nodo2 nodo2 : mejores) {
			System.out.println("Persona:" + ((int)nodo2.getPersona()+1) + " Trabajo: " + ((int)nodo2.getTrabajo()+1) + " Costo: " + nodo2.getKey());
		}
		
	}
	
	
	/**
	 * Convierte una lista de numeros en un vector basico
	 * @param java.util.List<Integer>  lista lista de numeros
	 * @return int[] vector de numeros
	 */
	private int[] toVectorBasic(List<Nodo2> lista)
	{
		//Se convierte la lista aun vector basico
		int[] vector = new int[lista.size()];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = lista.get(i).getKey();
			System.out.println(lista.get(i).getKey());
		}
		return vector;
		
	}
	
	private void armarArbol()
	{
		for (int trabajo = 0; trabajo < matrix.length; trabajo++) {
			for (int persona = 0; persona < matrix.length; persona++) {
				
				this.elarboL.addNodo2(this.matrix[trabajo][persona], persona, trabajo);				
			}
			
		}
	}
	
	
	private List<Nodo2> mejor(List<Nodo2> lista)
	{
		List<Nodo2> nlista = new ArrayList<Nodo2>();
		nlista.add(lista.get(0));
		
		while(lista.size() > 0){
			
		
			if(nlista.size() == 0 || this.factible(nlista, lista.get(0))){
				nlista.add(lista.get(0));			
			}
			lista.remove(0);
			
		}		
		return nlista;
	}
	
	private boolean factible(List<Nodo2> lista, Nodo2 nodo)
	{
		for (Nodo2 nodo2 : lista) {
			if(nodo.getPersona() == nodo2.getPersona() || nodo.getTrabajo() == nodo2.getTrabajo()){
				return false;
			}
		}
		return true;
	}
	
}
