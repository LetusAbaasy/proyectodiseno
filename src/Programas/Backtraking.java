package Programas;

public class Backtraking {
	
	protected int nPersonasTrabajos;
	protected int[][] matrizCostes;
	
	protected int minimo;
	protected int[] mejor;
	
	protected int[] solucion;
	
	public Backtraking(int npersonas,  int[][] costes)
	{
		this.nPersonasTrabajos = npersonas;
		this.matrizCostes = costes;
		this.solucion = new int[npersonas];			 //incializa Solucion
		this.solucion = new int[npersonas];
		this.minimo = Integer.MAX_VALUE;
	}
	
	public void asignacion(int etapa)
	{			
		solucion[etapa] = -1;
		
		System.out.println(solucion);
		
		do {
			solucion[etapa] = solucion[etapa] + 1;
			
			if(this.aceptable(etapa)) {
				if(( etapa + 1) < this.nPersonasTrabajos ) {
					this.asignacion(etapa + 1);
				}else{
					int coste = this.coste();
					if(this.minimo > coste){
						this.mejor = solucion;
						this.minimo = coste;
					}
				}
					
			}
		} while (solucion[etapa] !=  this.nPersonasTrabajos);
		
		
	}
	
	public boolean asigancion2(int etapa, int[] solucion)
	{
		System.out.println("Etapa:" + etapa);
		if(etapa >= this.nPersonasTrabajos){
			return false;
		}
		boolean exito = false;
		solucion[etapa] = -1;
		
		do {
			solucion[etapa] = solucion[etapa] +1;
			if(this.aceptable2(etapa,solucion)){
				if (etapa != this.nPersonasTrabajos){
					exito = asigancion2(etapa +1 , solucion);
				}else{
					exito =  true;
					int coste = this.coste2(solucion);
					if(this.minimo > coste){
						this.mejor = solucion;
						this.minimo = coste;
					}
				}
			}
		} while (!exito ||  ( solucion[etapa] != this.nPersonasTrabajos));
		return exito;
		
	}
	
	protected boolean aceptable2(int etapa, int[] solucion)
	{
		for (int i = 1; i < etapa; i++) {
			if(solucion[etapa] == solucion[i]) {
				return false;
			}
		}
		return true;
	}
	
	
	protected boolean aceptable(int etapa)
	{
		for (int i = 1; i < etapa; i++) {
			if(solucion[etapa] == solucion[i]) {
				return false;
			}
		}
		return true;
	}
	
	protected int coste2(int[] solucion)
	{
		int suma = 0;
		for (int i = 1; i <= this.nPersonasTrabajos; i++) {
			suma = suma + this.matrizCostes[i][solucion[i]];
		}
		return suma;
	} 
	protected int coste()
	{
		int suma = 0;
		for (int i = 1; i < this.nPersonasTrabajos; i++) {
			System.out.println(solucion[i]);
			suma = suma + this.matrizCostes[i][solucion[i]];
		}
		return suma;
	}
	
	public int[] getMejor()
	{
		return this.mejor;
	}
	
	public int[] getSolucion()
	{
		return this.solucion;
	}
	

}
