package Programas;

import java.util.Arrays;
import java.util.List;

import ArbolBinario.Arbol;
import ArbolBinario.Nodo;
import Estructura.Resultado;

/**
 * 
 * @author Juan Diaz - FuriosoJack <http://blog.furiosojack.com/>
 * 
 *  Division y recursion
 *
 */
public class DivideYVenceras implements Resultado
{
	private int[] arreglo;
	private String resultado;
	
	
	public DivideYVenceras(int[] arreglo)
	{
		this.arreglo = arreglo;		 
	}	

	/**
	 * Obtiene la lista de numeros ordenasdos de menor a mayor del vector 
	 * @return java.util.List<Integer> lista de numeros ordenados ;
	 */
	private List<Integer> getListaOrdernadaDeNumeros()
	{
		//Se hace el ordenamiento del vector
		Arbol elArbol = new Arbol();
		elArbol.setLista(this.arreglo);
		//Se le dice al arbol que recorra desde el nodo raiz
		elArbol.inOrden(elArbol.getRaiz());		
		return elArbol.getOrdenada();
	}
	
	/**
	 * Convierte una lista de numeros en un vector basico
	 * @param java.util.List<Integer>  lista lista de numeros
	 * @return int[] vector de numeros
	 */
	private int[] toVectorBasic(List<Integer> lista)
	{
		//Se convierte la lista aun vector basico
		int[] vector = new int[lista.size()];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = lista.get(i);
		}
		return vector;
		
	}
	
	/**
	 * Obtiene un Nodo que tiene como key el numero de veces que tiene el numero 
	 * y que a su vez el numero representa en el nodo el valor
	 * @param vector es arreglo de numeros
	 * @return Nodo es el mayor repetido del arreglo
	 */
	private Nodo mayorRepetido(int[] vector)
	{
		
		//Para este caso el nodo tendra como key el numero de repeticiones
		Nodo numero1 = new Nodo(0);
		// y  e valor el numero en si
		numero1.setValor(vector[0]);
		
		//Se recorre desde el primer elemento hasta una que sea diferente
		for (int i = 0; i < vector.length; i++) {			
			//Se verifica que el primer elemento sea el mismo en que se esta
			if(vector[0] == vector[i]) {
				numero1.setKey(numero1.getKey()+1);
			}else {
				//en este caso el emento acutal del vector es de diferente valor al primero
				
				//Se obtiene un subarreglo desde el elemento actual hasta el tamaño del vector
				int[] nuevoArreglo = Arrays.copyOfRange(vector, i, vector.length);
				//Se obtiene el nodo del numero que mas se repite del subvector
				Nodo numero2 = mayorRepetido(nuevoArreglo);
				
				//Se hace comparacion para retornar el nodo que tenga mas repeticiones del vector actual
				if(numero1.getKey() < numero2.getKey()) {
					return numero2;
				}
				return numero1;
			}
		}
		//este caso solo ocurre cuando:
		// * El vector tiene un elemento
		// * cuando el vector tiene todos los elementos del mismo tipo
		return numero1;
		
	}	
	
	/**
	 *  Se encarga de ejectar todos los procesos necesarios para obtener el resultado
	 * @return void
	 * 
	 */
	public void run()
	{
		
		//Se obtiene el numero que mas se repitio en el arreglo		
		Nodo elQueMasSeRepitido = this.mayorRepetido(
				this.toVectorBasic(
						this.getListaOrdernadaDeNumeros()
						)
				);
		
		//Se hace validcion si existe mayoritario
		if(elQueMasSeRepitido.getKey() > (this.arreglo.length/2)) {
			this.resultado = "El elemento mayoritarios es: " + elQueMasSeRepitido.getValor();			
		}else {
			this.resultado = "No existe elemento mayoritario";
		}		
		
	}

	@Override
	public String getResultado() {
		return this.resultado;
	}	

	
}
