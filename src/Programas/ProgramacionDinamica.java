package Programas;

/**
 * 
 * http://www.lcc.uma.es/~av/Libro/CAP5.pdf
 *
 */
public class ProgramacionDinamica {

	public static void ProgramacionDinamica(int[][] embarcaderos, int embarcaderoSalida, int embarcaderoLlegada, int cantidadEmbarcaderos){
		System.out.println("Matriz Inicial \n");
		verEmbarcaderos(embarcaderos);
		for(int j = 1; j < cantidadEmbarcaderos-1; j++){
			for(int k = j; k < cantidadEmbarcaderos-j; k++){
				embarcaderos[j][k+j] = minRecorrido(embarcaderos, embarcaderoSalida, embarcaderoLlegada);
			}
		}
		int res = minRecorrido(embarcaderos, embarcaderoSalida, embarcaderoLlegada);
		System.out.println("Datos:\n");
		System.out.println("Cantidad de embarcaderos: "+cantidadEmbarcaderos+" \n");
		System.out.println("Salida: #"+embarcaderoSalida+" \n");
		System.out.println("Llegada: #"+embarcaderoLlegada+" \n");
		System.out.println("Tabla Dinamica \n");
		verEmbarcaderos(embarcaderos);
		System.out.println("El coste minimo para ir de #"+embarcaderoSalida+" al #"+embarcaderoLlegada+" es de: "+res);
	}

	public static int minRecorrido(int[][] embarcaderos, int embarcaderoSalida, int embarcaderoLlegada){
	    int k,result,min=99999;
	    for(int i = 0; i < embarcaderos.length; i++) {
	    	for(int j = 0; j < embarcaderos.length; j++) {
	    		if(i >= j) {
	    			return 0;
	    		}
			    if(j-1 == i) {
			       return embarcaderos[i][j];
			    }else{
			        for(k=i+1;k<=j-1;k++){
			            result = minRecorrido(embarcaderos,embarcaderoSalida,k)+minRecorrido(embarcaderos,k,embarcaderoLlegada);
			            if(result < min)
			                min = result;
			        }
			    }
	    	}
	    }
	    return min;
	}
	
	public static int minimo2(int a, int b){
		int[][] r = new int[0][0];
		int min = 0;
		int[] c = new int[0];
		if(a < b){
			return b;
		}else{
			return a;
		}
		
		/*for(int i = a - 2; i>0; i++){
		    for(int j = b + 1; j < a; j++){
		       min=999999;
		       for(int k = i + 1; k < j - 1; k++){
		          r = C[i,k]+C[k,j];
		          if(r[i][j] < min)
		            min = r[i][j];
		       }   
		    }
		}*/
	}
	
	public static void verEmbarcaderos(int embarcaderos[][]){
		for (int i = 0; i < embarcaderos.length; i++) {
			for (int j = 0; j < embarcaderos[i].length; j++) {
				System.out.print(embarcaderos[i][j]+"\t");
			}
			System.out.println("\n");
		}
    }
}
